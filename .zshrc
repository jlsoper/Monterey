#!/bin/zsh


if [ $HOME != /root ]
then
  PS1="%~>"
else
  PS1="(%F{red}%n%f):%~>"
fi


alias vi='vim -i NONE -u NONE'  # \r  -->  ^M

